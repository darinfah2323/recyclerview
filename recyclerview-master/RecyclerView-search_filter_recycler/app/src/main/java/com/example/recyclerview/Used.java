package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class Used extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    List<String> moviesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        moviesList = new ArrayList<>();
        moviesList.add("Advertising 1 Day");
        moviesList.add("Advertising 7 Day");
        moviesList.add("Advertising 30 Day");
        moviesList.add("Advertising 1 Month");
        moviesList.add("Advertising 2 Month");
        moviesList.add("Advertising 3 Month");
        moviesList.add("Fan");
        moviesList.add("microwave");
        moviesList.add("Refrigerator");
        moviesList.add("Television");
        moviesList.add("Phone");
        moviesList.add("Motorcycle");
        moviesList.add("Car");

        recyclerView = findViewById(R.id.recyclerView);
        recyclerAdapter = new RecyclerAdapter(moviesList);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
    }


}